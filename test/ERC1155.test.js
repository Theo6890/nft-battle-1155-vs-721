// Created contracts
const CompanyEmployeesCards = artifacts.require("CompanyEmployeesCards");
//Module loading
require("chai").use(require("chai-as-promised")).should();
const { expect } = require("chai");
const truffleAssert = require("truffle-assertions");
const Utils = require("./Utils.js");

let jobs;
contract("CompanyEmployeesCards", (accounts) => {
  before(async () => {
    cards = await CompanyEmployeesCards.new();
  });

  describe("Employee cards issuance", async () => {
    it("adds a FOUNDER", async () => {
      await cards.addEmployee("Thor", accounts[0], 0, "ezrie-uilzef");
      const balance = await cards.balanceOf(accounts[0], 0);
      expect(parseFloat(balance), 1);
    });

    it("fails adding a second FOUNDER", async () => {
      await truffleAssert.fails(
        cards.addEmployee("Loki", accounts[1], 0, "locki-eztq"),
        " Current job fully occupied"
      );
    });

    it("adds 3 devs", async () => {
      const names = ["SpaceCowboy", "Marie Poppins", "CharlatanMan"];
      const linkedIn = ["SpaceCowboy", "Marie Poppins", "CharlatanMan"];

      for (i = 0; i < 3; i++) {
        await cards.addEmployee(names[i], accounts[i + 1], 1, linkedIn[i]);

        let balance = await cards.balanceOf(accounts[i + 1], 1);
        balance = parseFloat(balance);

        expect(balance).to.be.eq(1);
      }
    });

    it("fails creating a second to an already employed dev", async () => {
      await truffleAssert.fails(
        cards.addEmployee("WonderWoman", accounts[1], 1, "WonderWoman-eztq"),
        "Already employed"
      );
    });

    it("gets info about a specific employee", async () => {
      const result = await cards.getEmployeeInfo(1);
      console.log(result);
    });

    it("gets all employees", async () => {
      const result = await cards.getAllEmployees();
      console.log(result);
    });
  });
});
