// Created contracts
const InternetMasterSwitch = artifacts.require("InternetMasterSwitch");
const Mewtwo = artifacts.require("Mewtwo");
//Module loading
require("chai").use(require("chai-as-promised")).should();
const truffleAssert = require("truffle-assertions");
const Utils = require("./Utils.js");

contract("InternetMasterSwitch", (accounts) => {
  before(async () => {
    internetSwitch = await InternetMasterSwitch.new();
  });

  describe("InternetMasterSwitch actions", async () => {
    it("transfers to accounts[1]", async () => {
      await truffleAssert.passes(
        internetSwitch.safeTransferFrom(accounts[0], accounts[1], 1)
      );

      let balance = await internetSwitch.balanceOf(accounts[1]);
      balance = parseFloat(balance);
      expect(balance).to.be.eq(1);
    });

    it("accounts[1] tries to kill internet", async () => {
      await truffleAssert.fails(
        internetSwitch.nukeInternet({ from: accounts[1] }),
        "Nope sorry padawan, you aren't the Grand Internet Master"
      );
    });

    it("accounts[0] tries to kill internet", async () => {
      await truffleAssert.fails(
        internetSwitch.nukeInternet(),
        "You forgot the key at home dude!"
      );
    });

    it("transfers to accounts[0]", async () => {
      await internetSwitch.approve(accounts[0], 1, { from: accounts[1] });
      await internetSwitch.safeTransferFrom(accounts[1], accounts[0], 1);
      await truffleAssert.passes(internetSwitch.nukeInternet());
    });
  });
});

contract("Mewtwo", (accounts) => {
  before(async () => {
    mewtwo = await Mewtwo.new();
  });

  describe("Mewtwo actions", async () => {
    it("verifies deployer can't chase Mewtwo again", async () => {
      await truffleAssert.fails(
        mewtwo.chaseMewtwo(),
        "Nope sorry mate, you've already captured the lengendary Mewtwo"
      );

      let balance = await mewtwo.balanceOf(accounts[0]);
      balance = parseFloat(balance);
      expect(balance).to.be.eq(1);
    });

    it("accounts[1] tries to chase Mewtwo", async () => {
      await mewtwo.chaseMewtwo({ from: accounts[1] });
    });
  });
});
