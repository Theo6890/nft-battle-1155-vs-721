// SPDX-License-Identifier: MIT
pragma solidity ^0.8.1;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract CompanyEmployeesCards is ERC1155 {
    using Counters for Counters.Counter;
    Counters.Counter private _employeeIds;

    enum JOB {FOUNDER, DEV}

    mapping(JOB => uint256) private _jobMaxSupply;
    mapping(JOB => uint256) private _jobCurrentSupply;
    mapping(address => bool) private _isAlreadyEmployed;

    struct Employee {
        uint256 id;
        string superheroName;
        address wallet;
        JOB role;
        string linkedInTag;
    }

    Employee[] private employees;

    event EmployeeAdded(Employee e, address recruiter);

    modifier isJobAvailable(JOB job) {
        require(
            _jobCurrentSupply[job] < _jobMaxSupply[job],
            "Current job fully occupied"
        );
        _;
    }

    modifier onlyOneCardPerEmployee(address wallet) {
        require(!_isAlreadyEmployed[wallet], "Already employed");
        _;
    }

    constructor() ERC1155("https://www.linkedin.com/in/{linkedInTag}") {
        _jobMaxSupply[JOB.FOUNDER] = 1;
        _jobMaxSupply[JOB.DEV] = 10**6;
    }

    function addEmployee(
        string memory superheroName,
        address employeeWallet,
        JOB job,
        string memory linkedInTag
    ) public onlyOneCardPerEmployee(employeeWallet) {
        _employeeIds.increment();

        Employee memory e =
            Employee(
                _employeeIds.current(),
                superheroName,
                employeeWallet,
                job,
                linkedInTag
            );

        employees.push(e);

        issueEmployeeCard(employeeWallet, job);

        emit EmployeeAdded(e, msg.sender);
    }

    function issueEmployeeCard(address employeeWallet, JOB job)
        private
        isJobAvailable(job)
    {
        _mint(employeeWallet, uint256(job), 1, "");
        _jobCurrentSupply[job]++;
        _isAlreadyEmployed[employeeWallet] = true;
    }

    function getEmployeeInfo(uint256 id)
        public
        view
        returns (
            string memory superheroName,
            address wallet,
            JOB role,
            string memory linkedInTag
        )
    {
        superheroName = employees[id - 1].superheroName;
        wallet = employees[id - 1].wallet;
        role = employees[id - 1].role;
        linkedInTag = employees[id - 1].linkedInTag;
    }

    function getAllEmployees() public view returns (Employee[] memory) {
        return employees;
    }
}
