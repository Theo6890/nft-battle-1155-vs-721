// SPDX-License-Identifier: MIT
pragma solidity ^0.8.1;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract InternetMasterSwitch is ERC721 {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenId;

    address public deployer = _msgSender();

    modifier onlyDeployer() {
        // solhint-disable-next-line reason-string
        require(
            _msgSender() == deployer,
            "Nope sorry padawan, you aren't the Grand Internet Master"
        );
        _;
    }

    modifier hasTheKey() {
        // solhint-disable-next-line reason-string
        require(
            balanceOf(_msgSender()) == 1,
            "You forgot the key at home dude!"
        );
        _;
    }

    constructor() ERC721("Internet Master Switch", "IMS") {
        deployer = _msgSender();
        _tokenId.increment();

        _mint(deployer, _tokenId.current());
    }

    function _baseURI() internal pure override returns (string memory) {
        return "QmQVfxtpHajUHwfDyzsnMb7EV41CBWeaB4KgV9fmqUw2Eh";
    }

    function nukeInternet() external onlyDeployer hasTheKey {
        selfdestruct(payable(address(this)));
    }
}
