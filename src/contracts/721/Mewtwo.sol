// SPDX-License-Identifier: MIT
pragma solidity ^0.8.1;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract Mewtwo is ERC721 {
    using Counters for Counters.Counter;
    Counters.Counter private _mewtwoIds;

    modifier onlyIfNotAlreadyOwned() {
        // solhint-disable-next-line reason-string
        require(
            balanceOf(_msgSender()) == 0,
            "Nope sorry mate, you've already captured the lengendary Mewtwo"
        );
        _;
    }

    modifier canBeCaptured() {
        // solhint-disable-next-line reason-string
        require(
            capture(),
            "Looks like Mewtwo does not want to be captured atm"
        );
        _;
    }

    constructor() ERC721("Mewtwo pokemon", "MEW2") {
        _mewtwoIds.increment();
        _mint(_msgSender(), _mewtwoIds.current());
    }

    function _baseURI() internal pure override returns (string memory) {
        return "QmeVe9YNBC2DEbZQ3vyATTXSrsPf1aiaK7fXBbrweF7sTT";
    }

    function chaseMewtwo() external onlyIfNotAlreadyOwned canBeCaptured {
        _mewtwoIds.increment();
        _mint(_msgSender(), _mewtwoIds.current());
    }

    function capture() public view returns (bool) {
        return block.timestamp % 2 == 0;
    }
}
